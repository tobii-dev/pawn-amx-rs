# PAWN Abstract Machine in Rust
// Very early stages, I'm still learning Rust :)

## Description
Most basic Rust implementation of a PAWN bytecode interpreter.
Should be able to run any P-code .amx file in safe Rust.
(.amxx File version 7+, 32bit cells)
Not meant to be fast or embedable, just a proof of concept.


## Getting started

[PAWN](https://github.com/compuphase/pawn)

[Pawn_Implementer_Guide](https://github.com/compuphase/pawn/blob/master/doc/Pawn_Implementer_Guide.pdf)

[amx.c](https://github.com/compuphase/pawn/blob/master/amx/amx.c)

[amx.rs](https://github.com/Pycckue-Bnepeg/samp-rs/tree/master/samp-sdk/src)


### Installation
```bash
https://gitlab.com/tobii-dev/pawn-amx-rs
cd pawn-amx-rs
cargo build --release
```

### Usage
```bash
cargo build --release -- hello_world.amx
```


***

## Roadmap
//

### TODO
- [ ] Run hello world...


## Contributing
//

## Support
//

## Authors and acknowledgment
//

## License
//

## Project status
//
